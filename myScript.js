// определяем элементы блока <form>
var formElems = document.getElementsByTagName('form')[0];
var emailLabel = formElems.children[0];
var pswLabel = formElems.children[1];
var singUpButton = formElems.children[2];
var preloader = formElems.children[3];
var errorDiv = formElems.children[4];

// определяем инпуты
var inputs = document.getElementsByTagName('input');
var emailInput = inputs[0];
var pswInput = inputs[1];

// определяем элементы блока <div>, в которые будет выводиться информация
var divElems = formElems.nextElementSibling;
var image = divElems.children[0];
var spanName = divElems.children[1];
var spanCoutry = divElems.children[2];
var spanHobbies = divElems.children[3];
var singOutButton = divElems.children[4];

// назначаем обработчики для кнопок
singUpButton.addEventListener('click',clickSingUpButton);
singOutButton.addEventListener('click',clickSingOutButton);

function clickSingUpButton(event){
    event.preventDefault();          // отменяю действие по умолчанию
    
    var xhr = new XMLHttpRequest();
    var body = 'email=' + encodeURIComponent(emailInput.value) + '&password=' + encodeURIComponent(pswInput.value);
    xhr.open("POST", 'http://netology-hj-ajax.herokuapp.com/homework/login_json', true);
    xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    xhr.send(body);

    xhr.onreadystatechange = function() {
        if(xhr.readyState != 4) return;

        // елементы формы становятся видны, а сообщение прелоадера убирается
        preloader.innerHTML = '';
        emailLabel.style.display = '';
        pswLabel.style.display = '';
        singUpButton.style.display = '';

        var str;
        if(xhr.status != 200){
           // ошибка
            str = xhr.responseText;
            str = JSON.parse(str);
            errorDiv.innerHTML = 'Error ' + str.error.code + ": " + str.error.message;
        }
        else{
            // результат
            preloader.innerHTML = '';
            emailLabel.style.display = 'none';
            pswLabel.style.display = 'none';
            singUpButton.style.display = 'none';

            var str = xhr.responseText;
            str = JSON.parse(str);
            image.setAttribute('src', str.userpic);
            spanName.innerHTML = '<br>Name: ' + str.name + ' ' + str.lastname + '<br>';
            spanCoutry.innerHTML = 'Country: ' + str.country + '<br>';
            spanHobbies.innerHTML = 'Hobbies: ' + str.hobbies.join(', ') + '<br>';
        }
    }

    // не смог понять, почему неправильном выполнении запроса не происходит вызов обработчика события error
    /*xhr.onload = function() {
        // результат
        preloader.innerHTML = '';
        emailLabel.style.display = 'none';
        pswLabel.style.display = 'none';
        singUpButton.style.display = 'none';
        var str = xhr.responseText;
        console.log('Строка str: ' + str);
        str = JSON.parse(str);
        console.log('Объект str: ' + str);
        image.setAttribute('src', str.userpic);
        spanName.innerHTML = '<br>Name: ' + str.name + ' ' + str.lastname + '<br>';
        spanCoutry.innerHTML = 'Country: ' + str.country + '<br>';
        spanHobbies.innerHTML = 'Hobbies: ' + str.hobbies.join(', ') + '<br>';
    }

    xhr.onerror = function() {
        // ошибка
        errorDiv.innerHTML = "Ошибка! " + xhr.status + ": " + xhr.statusText;
    }
    */
			
    // елементы формы становятся невидимыми, а прелоадер выдает сообщение 'Loading...'
	preloader.innerHTML = 'Loading...';
	errorDiv.innerHTML = '';
	emailLabel.style.display = 'none';
	pswLabel.style.display = 'none';
    singUpButton.style.display = 'none';
}
		
function clickSingOutButton(event){
    // сброс в начальное состояние элементов формы
	emailLabel.style.display = '';
	pswLabel.style.display = '';
    emailInput.value = '';
    pswInput.value = '';
	singUpButton.style.display = '';
    errorDiv.innerHTML = '';
    
    // сброс в начальное состояние элементов блока <div> для вывода информации
    image.removeAttribute('src');
    spanName.innerHTML = '';
    spanCoutry.innerHTML = '';
    spanHobbies.innerHTML = '';
}